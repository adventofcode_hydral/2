int fight(string round)
{
	if (round == "A X")
		return 4;
    if (round == "A Y")
		return 8;
    if (round == "A Z")
		return 3;
    if (round == "B X")
		return 1;
    if (round == "B Y")
		return 5;
    if (round == "B Z")
		return 9;
    if (round == "C X")
		return 7;
    if (round == "C Y")
		return 2;
    if (round == "C Z")
		return 6;
    else
		return 0;
}

void	main()
{
	var stream = FileStream.open("input", "r");
	var score = 0;

	while (!stream.eof())
	{
		var round = stream.read_line() ?? "";
		print("%s\n", round);
		score += fight (round);
	}
	print("%d", score);
}
