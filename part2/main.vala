int fight(string round)
{
	var score = 0;
	var s = round.split(" ");
	var j2 = s[0][0];
	
	if (s[1] == "X")
	{
		if (j2 == 'A')
			score += 3;
		if (j2 == 'B')
			score += 1;
		if (j2 == 'C')
			score += 2;
	}
	if (s[1] == "Y")
		score += 3 + (j2 - 64);
	if (s[1] == "Z")
	{
		score += 6;
		if (j2 == 'A')
			score += 2;
		if (j2 == 'B')
			score += 3;
		if (j2 == 'C')
			score += 1;
	}
	return score;
}

void	main()
{
	var stream = FileStream.open("input", "r");
	var score = 0;

	while (!stream.eof())
	{
		var round = stream.read_line() ?? " ";
		score += fight (round);
	}
	print("%d", score);
}
